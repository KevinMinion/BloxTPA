package com.bloxgaming.spigot.bloxtpa

import com.google.common.cache.Cache
import com.google.common.cache.CacheBuilder
import org.bukkit.entity.Player

/**
 * TPACache
 * @author Gregory Maddra
 * 2017-06-16
 */
val PendingTPACache: Cache<Player, Player> = CacheBuilder.newBuilder()
        .build<Player, Player>()
