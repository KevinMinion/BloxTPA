package com.bloxgaming.spigot.bloxtpa.commands

import com.bloxgaming.spigot.bloxtpa.BloxTPA
import com.bloxgaming.spigot.bloxtpa.Constants
import com.bloxgaming.spigot.bloxtpa.PendingTPACache
import org.bukkit.ChatColor
import org.bukkit.Location
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.command.ConsoleCommandSender
import org.bukkit.entity.Player
import org.bukkit.scheduler.BukkitRunnable

/**
 * TPAccept
 * @author Gregory Maddra
 * 2017-06-16
 */
class TPAccept : CommandExecutor {

    override fun onCommand(sender: CommandSender?, command: Command?, label: String?, args: Array<out String>): Boolean {
        if (sender is ConsoleCommandSender) {
            sender.sendMessage(Constants.ONLY_PLAYERS_ERROR)
        } else if (sender is Player) {
            if (args.isNotEmpty()) {
                return false
            }
            if (!sender.hasPermission(Constants.ACCEPT_TPA_PERMISSION)) {
                sender.sendMessage(Constants.NO_PERMISSION_ERROR)
                return true
            }
            val target = PendingTPACache.getIfPresent(sender)
            if (target == null) {
                sender.sendMessage("${ChatColor.RED}No pending requests!")
                return true
            }
            target.sendMessage("${ChatColor.GREEN}TPA Accepted. Remain still for ${ChatColor.WHITE}${BloxTPA.teleportDelay} " +
                    "${ChatColor.GREEN} seconds")
            var originalLoc: Location? = null
            object : BukkitRunnable() {
                override fun run() {
                    originalLoc = target.location
                }
            }.runTaskLater(BloxTPA.plugin, 20L) //20 ticks == 1 second
            sender.sendMessage("${ChatColor.GREEN}TPA Accepted.")
            PendingTPACache.invalidate(sender)
            object : BukkitRunnable() {
                override fun run() {
                    if (target.location == originalLoc) {
                        target.teleport(sender)
                    } else {
                        target.sendMessage("${ChatColor.RED} Movement detected! Teleportation aborted!")
                        sender.sendMessage("${target.playerListName} moved! Teleport canceled!")
                    }
                }
            }.runTaskLater(BloxTPA.plugin, 20L * BloxTPA.teleportDelay)
        }
        return true
    }
}