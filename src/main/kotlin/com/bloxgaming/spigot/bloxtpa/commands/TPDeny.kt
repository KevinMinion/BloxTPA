package com.bloxgaming.spigot.bloxtpa.commands

import com.bloxgaming.spigot.bloxtpa.Constants
import com.bloxgaming.spigot.bloxtpa.PendingTPACache
import org.bukkit.ChatColor
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.command.ConsoleCommandSender
import org.bukkit.entity.Player

/**
 * TPDeny
 * @author Gregory Maddra
 * 2017-06-16
 */
class TPDeny : CommandExecutor {

    override fun onCommand(sender: CommandSender?, command: Command?, label: String?, args: Array<out String>?): Boolean {
        if (sender is ConsoleCommandSender) {
            sender.sendMessage(Constants.ONLY_PLAYERS_ERROR)
        } else if (sender is Player) {
            val target = PendingTPACache.getIfPresent(sender)
            if (target == null) {
                sender.sendMessage("${ChatColor.RED}No pending requests!")
                return true
            }
            sender.sendMessage("${ChatColor.GREEN}TPA Denied!")
            target.sendMessage("${ChatColor.RED}TPA Request was denied!")
            PendingTPACache.invalidate(sender)
        }
        return true
    }
}